# -*- coding: utf-8 -*-

from cms.menu_bases import CMSAttachMenu
from menus.base import Menu, NavigationNode
from menus.menu_pool import menu_pool
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from polls.models import Poll


class PollsMenu(CMSAttachMenu):
    name = _("Polls Menu")  # nazwa menu

    def get_nodes(self, request):
        """
        Budowa drzewa menu.
        """
        nodes = []
        for poll in Poll.objects.all():
            # Drzewo menu składa się z instancji NavigationNode
            # Każdy NavigationNode przyjmuje etykietę jako swój pierwszy parametr,
            # URL jako swój drugi parametr oraz unikalne id jako trzeci
            node = NavigationNode(
                poll.question,
                reverse('poll_detail', args=(poll.pk,)),
                poll.pk
            )
            nodes.append(node)
        return nodes

menu_pool.register_menu(PollsMenu)  # rejestracja menu

# -*- coding: utf-8 -*-
from cms.models import CMSPlugin
from django.core.urlresolvers import reverse
from django.db import models
from django.utils import timezone


class Poll(models.Model):
    question = models.CharField('pytanie', max_length=200)
    pub_date = models.DateTimeField('termin publikacji')

    def __unicode__(self):
        return self.question

    def published_today(self):
        now = timezone.now()
        time_delta = now - self.pub_date
        return time_delta.days == 0
    published_today.boolean = True
    published_today.short_description = 'opublikowany dzisiaj?'

    def get_absolute_url(self):
        return reverse('poll_detail', args=[str(self.id)])


class Choice(models.Model):
    poll = models.ForeignKey(Poll)
    choice = models.CharField(u'odpowiedź', max_length=200)
    votes = models.IntegerField(u'liczba głosów', default=0)

    def __unicode__(self):
        return self.choice


class PollPlugin(CMSPlugin):
    poll = models.ForeignKey(Poll)

    def __unicode__(self):
      return self.poll.question
# -*- coding: utf-8 -*-

from django.utils.translation import ugettext as _
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from .models import PollPlugin as PollPluginModel


class PollPlugin(CMSPluginBase):
    model = PollPluginModel  # model, który przechowuje dane o wtyczce
    name = _("Poll Plugin")  # nazwa wtyczki
    render_template = "polls/plugin.html" # szablon do renderowania pluginu

plugin_pool.register_plugin(PollPlugin) # rejestracja wtyczki
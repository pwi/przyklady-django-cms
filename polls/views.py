# -*- coding: utf-8 -*-

from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _

from .models import Poll, Choice


def vote_view(request, pk):
    poll = get_object_or_404(Poll, pk=pk)
    if request.method == "POST":
        try:
            # sprawdzenie wyboru
            # walidacja po stronie serwera jest zawsze konieczna!
            choice = poll.choice_set.get(
                        pk=request.POST.get('choice', 0))
        except Choice.DoesNotExist: # błąd — odesłanie widoku szczegółowego
            msg = u"Ups, wybierz odpowiedź, która istnieje"
            messages.add_message(request, messages.ERROR, msg)
            url = reverse('poll_detail', args=[pk, ])
        else: # zapisanie głosu i odesłanie wyników
            choice.votes += 1
            choice.save()
            messages.add_message(request, messages.INFO,
                                 u"Zagłosowałeś na %s" % choice)
            url = reverse('poll_result', args=[pk])
    else: # wysłane metodą GET — ignorujemy
        url = reverse('poll_detail', args=[pk, ])
    # przekierowanie
    return HttpResponseRedirect(redirect_to=url)



def detail(request, poll_id):
    poll = get_object_or_404(Poll, pk=poll_id)
    request.toolbar.populate()
    menu = request.toolbar.get_or_create_menu('polls-app', _('Polls'))
    menu.add_modal_item(_('Change this Poll'), url=reverse('admin:polls_poll_change', args=[poll_id]))
    menu.add_sideframe_item(_('Show History of this Poll'), url=reverse('admin:polls_poll_history', args=[poll_id]))
    menu.add_sideframe_item(_('Delete this Poll'), url=reverse('admin:polls_poll_delete', args=[poll_id]))

    return render(request, 'polls/detail.html', {'poll': poll})

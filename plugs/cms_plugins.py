# -*- coding: utf-8 -*-

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext_lazy as _

from .models import Hello


class HelloPlugin(CMSPluginBase):
    model = Hello
    name = _("Wtyczka powitalna")
    render_template = "hello_plugin.html"

    # domyślne działanie metody render
    #def render(self, context, instance, placeholder):
    #   context['instance'] = instance
    #   context['placeholder'] = placeholder
    #   return context

plugin_pool.register_plugin(HelloPlugin)
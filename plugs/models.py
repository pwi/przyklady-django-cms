# -*- coding: utf-8 -*-

from cms.models.pluginmodel import CMSPlugin
from django.db import models


class Hello(CMSPlugin):
    guest_name = models.CharField(u'Nazwa gościa', max_length=50, default=u'Gościu')
